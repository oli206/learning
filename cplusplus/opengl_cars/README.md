Possible problems to run the project (and fixes):

1>glaux.lib(tk.obj): error LNK2019: unresolved external symbol _sscanf, which is referenced in function _GetRegistrySysColors@8
1>glaux.lib(tk.obj): error LNK2019: unresolved external symbol _vsprintf, the symbol is referenced in function _PrintMessage
VS2015 compilation will cause this problem. The solution is to add the dependency legacy_stdio_definitions.lib in the project properties -> linker -> input -> additional dependencies;
