import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.Arrays;

/**
 * Created by Oliver on 21/08/14.
 */
public class Main {

    static String encryptionKey = /* ?????? */ "00000000000000000000000000";
    static String IV = "DkBbcmQo1QH+ed1wTyBynA==";
    static String ciphertext = "yptyoDdVBdQtGhgoePppYHnWyugGmy0j81sf3zBeUXEO/LYRw+2XmVa0/v6YiSy9Kj8gMn/gNu2I7dPmfgSEHPUDJpNpiOWmmW1/jw/Pt29Are5tumWmnfkazcAb23xe7B4ruPZVxUEhfn/IrZPNZdr4cQNrHNgEv2ts8gVFuOBU+p792UPy8/mEIhW5ECppxGIb7Yrpg4w7IYNeFtX5d9W4W1t2e+6PcdcjkBK4a8y1cjEtuQ07RpPChOvLcSzlB/Bg7UKntzorRsn+y/d72qD2QxRzcXgbynCNalF7zaT6pEnwKB4i05fTQw6nB7SU1w2/EvCGlfiyR2Ia08mA0GikqegYA6xG/EAGs3ZJ0aQUGt0YZz0P7uBsQKdmCg7jzzEMHyGZDNGTj0F2dOFHLSOTT2/GGSht8eD/Ae7u/xnJj0bGgAKMtNttGFlNyvKpt2vDDT3Orfk6Jk/rD4CIz6O/Tnt0NkJLucHtIyvBYGtQR4+mhbfUELkczeDSxTXGDLaiU3de6tPaa0/vjzizoUbNFdfkIly/HWINdHoO83E="; /*Note null padding*/

    private void execute() throws Exception {

        Cipher cipherDecrypt = Cipher.getInstance("AES/CBC/NoPadding");

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("C:\\decrypt.txt", true)))) {
            for (int a = 0; a < 10; a++){
                for (int b = 0; b < 10; b++) {
                    for (int c = 0; c < 10; c++){
                        for (int d = 0; d < 10; d++){
                            for (int e = 0; e < 10; e++){
                                for (int f = 0; f < 10; f++){
                                    String key = ""+a+b+c+d+e+f+encryptionKey;
                                    cipherDecrypt.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(),"AES"), new IvParameterSpec(DatatypeConverter.parseBase64Binary(IV)));
                                    byte[] textBytes = cipherDecrypt.doFinal(DatatypeConverter.parseBase64Binary(ciphertext));
                                    String plaintext = new String(textBytes, "UTF-8");
                                    System.out.println("decrypted text : "+plaintext);
                                    out.println(Arrays.toString(textBytes));
                                }
                            }
                        }
                    }
                }
            }
        }catch (IOException e) {
            //exception handling left as an exercise for the reader
        }


    }

    public static void main(String[] args) throws Exception {
        Main testingAES = new Main();
        testingAES.execute();
    }

}
