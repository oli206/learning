package hw;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface HelloWorld
{
    String sayHi(@WebParam(name="firstName") String firstName);
}