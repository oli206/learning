import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Oliver on 1/09/14.
 */
    public class TestJPanel implements ActionListener
    {
        private JButton button;
        private JTextArea textArea;
        private JCheckBox checkBox;

        public TestJPanel() {
            button = new JButton("I'm a button");
            button.addActionListener(this);

            checkBox = new JCheckBox();
            checkBox.addActionListener(this);

            textArea = new JTextArea();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == button)
            {
                textArea.setText("Button clicked");
            }
            else if (e.getSource() == checkBox)
            {
                textArea.setText("Checkbox clicked");
            }
        }
    }
