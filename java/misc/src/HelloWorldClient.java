import hw.HelloWorld;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;


public final class HelloWorldClient
{

    private static final QName SERVICE_NAME = new QName("http://server.hw.demo/",   "HelloWorld");
    private static final QName PORT_NAME = new QName("http://server.hw.demo/",    "HelloWorldPort");

    private HelloWorldClient()
    {
    }

    public static void main(String args[]) throws Exception
    {
        Service service = Service.create(SERVICE_NAME);
        String endpointAddress = "http://localhost:9000/helloWorld?wsdl";
        // If web service deployed on Tomcat deployment, endpoint should be changed
        // to:
        // String 
//      endpointAddress =
//       "http://localhost:8080/java_first_jaxws/services/hello_world";

        // Add a port to the Service
        service.addPort(PORT_NAME, SOAPBinding.SOAP11HTTP_BINDING, endpointAddress);

        HelloWorld hw = service.getPort(HelloWorld.class);
        System.out.println(hw.sayHi("Albert"));

    }

}