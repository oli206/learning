package hwimpl;

import javax.jws.WebService;

@WebService(endpointInterface = "hw.HelloWorld", serviceName = "HelloWorld")
public class HelloWorldImpl
{
    static public void say(String msg) { System.out.println(msg); }

    public String sayHi(String firstName)
    { say ("sayHi called with " + firstName);
        return "Hello " + firstName + " from the World.";
    }
}